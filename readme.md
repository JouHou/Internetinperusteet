A school project for University of Oulu's Basics of Internet course. 

Implements:
- one-time pad encryption
- multipart messages
- parity checking
