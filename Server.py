# Basic server program to aid in testing the client 

import logging
import socket
import struct
import random

randomwordlist = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

def main():

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ("127.0.0.1", 10000)

    print("Starting to listen at: " + server_address[0] + " (port " + str(server_address[1]) + ").")

    sock.bind(server_address)
    sock.listen(1)
    connection, client_address = sock.accept()
    udpport = random.randint(5000, 10001)

    if connection:
        if connection.recv(4096).decode() == "HELLO":
            print("Client says: HELLO")
            msg = "HELLO QWERTYUIOP {}\r\n".format(udpport)
            connection.sendall(msg.encode())
            sock.close()
            udp_server(port=udpport)


def udp_server(host='127.0.0.1', port=random.randint(5000, 10000)):
    udpsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    print("Listening on udp %s:%s" % (host, port))
    udpsock.bind((host, port))
    msglimit = random.randint(1, 10)
    eom = False
    j = 0
    for i in range(msglimit):
        (datagram, addr) = udpsock.recvfrom(1024)
        if datagram:
            j += 1
        cid, ack, eom, dr, cl, payload = struct.unpack('!8s??HH128s', datagram)
        print("Client says: " + payload.decode()[0:cl])
        randomwords = []
        if i == msglimit - 1:
            eom = True
        for i in range(random.randint(1, 5)):
            randomwords.append(random.choice(randomwordlist))
        message = str(" ".join(randomwords))
        if eom:
            message = "You replied to {} messages. Bye".format(j-1)
        datatoclient = struct.pack('!8s??HH128s', "SERVER".encode(), True, eom, 0, len(message), message.encode())
        udpsock.sendto(datatoclient, addr)


if __name__ == '__main__':
    # Call the main function when this script is executed
    main()
