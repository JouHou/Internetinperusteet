#!/usr/bin/python
# -*- coding: utf-8 -*-
 
# The modules required
import sys
import os
import binascii
import socket
import struct

'''
Usage: python3 CourseWorkTemplate.py <server address> <server port> <message>
For example: python3 CourseWorkTemplate.py xxx.xx.x.xxx 10000 "HELLO ENC MUL PAR"
''' 


def send_and_receive_tcp(address, port, message):
    print("You gave arguments: {} {} {}".format(address, port, message))
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect((address, port))
    except ConnectionRefusedError:
        print("Connection refused.")
        return

    message += "\r\n"  # Carriage return and line feed before the encryption keys.
    global clientkeylist
    clientkeylist = []

    # Generate the encryption keys and add them to the key list and message.
    for i in range(20):
        secret = binascii.b2a_hex(os.urandom(32)).decode()
        message += "{}{}".format(secret, "\r\n")
        clientkeylist.append(secret)

    # Add newline, send the handshake message and receive the answer.
    message += ".\r\n"
    sock.sendall(message.encode())
    recv = sock.recv(4096).decode()

    # Parse the answer. Result is the server encryption key list.
    global serverkeylist
    serverkeylist = recv.splitlines()
    _, cid, udpport = serverkeylist[0].split(" ")
    serverkeylist.pop(0)  # First line contains the client id and UDP port.
    serverkeylist.pop(20)  # Last line contains nothing useful.

    # Close the TCP socket and initiate UDP messaging.
    sock.close()
    send_and_receive_udp(address, udpport, cid)
    return
 
 
def send_and_receive_udp(address, udpport, cid):
    udpsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udpsock.bind(("130.231.241.32", int(udpport)))
    msg = "Hello from {}\r\n".format(cid)

    # First encrypt, then add the parity, pack and send.
    message = add_parity(encryption(msg, clientkeylist))
    data = struct.pack('!8s??HH128s', cid.encode(), True, False, 0, len(message), message.encode())
    udpsock.sendto(data, (address, int(udpport)))

    # Loop for receiving and sending the (multipart) UDP messages.
    while True:
        payload = ""
        parity = True
        while True:
            datagram, addr = udpsock.recvfrom(4096)
            sid, ack, eom, dr, cl, messagepart = struct.unpack('!8s??HH128s', datagram)

            # If EOM bit is True, then only print the message and exit.
            if eom:
                print(messagepart.decode()[0:cl])
                udpsock.close()
                return

            # If parity check fails, raise the wrong parity flag and remove a server encryption key (if any left).
            if not check_parity(messagepart.decode()[0:cl]):
                parity = False
                if serverkeylist:
                    serverkeylist.pop(0)
            # Else, remove the parity and encryption and append the part to message.
            else:
                if serverkeylist:
                    payload += encryption(check_parity(messagepart.decode()[0:cl]), serverkeylist)
                else:
                    payload += check_parity(messagepart.decode()[0:cl])
            # If data remaining is 0, break out of the multipart receiving loop.
            if dr == 0:
                break

        # If parity check fails, send a message with ACK bit as False and content as "Send again".
        if not parity:
            message = "Send again"

            # If there are keys left, then encrypt and then add the parity. Else, only add parity.
            if clientkeylist:
                message = add_parity(encryption(message, clientkeylist))
            else:
                message = add_parity(message)

            data = struct.pack('!8s??HH128s', cid.encode(), False, False, 0, len(message), message.encode())
            udpsock.sendto(data, (address, int(udpport)))

        # Else if the parity check is correct, send a message with ACK bit as True and reverse the content.
        else:
            wordlist = []
            for word in payload.split(" "):
                wordlist.append(word)
            wordlist.reverse()
            message = " ".join(wordlist)

            # Split the message into pieces, add encryption and parity to each piece and then send them one at a time.
            remain = len(message)
            for piece in pieces(message):
                remain -= len(piece)
                # If there are keys left, then encrypt and then add the parity. Else, only add parity
                if clientkeylist:
                    piece = add_parity(encryption(piece, clientkeylist))
                else:
                    piece = add_parity(piece)
                data = struct.pack('!8s??HH128s', cid.encode(), True, False, remain, len(piece), piece.encode())
                udpsock.sendto(data, (address, int(udpport)))


# Function for encrypting and decrypting of the messages. Borrowed from course materials.
def encryption(string, keylist):
    newstring = ""
    for i in range(len(string)):
        newstring += chr(ord(string[i]) ^ ord(keylist[0][i % 64]))
    keylist.pop(0)
    return newstring


# Function for calculating the parity number of a character. Borrowed from course materials.
def get_parity(n):
    while n > 1:
        n = (n >> 1) ^ (n & 1)
    return n


# Function for adding the parity to the messages to be sent.
def add_parity(string):
    newstring = ""
    for char in string:
        c = ord(char)
        c <<= 1
        c += get_parity(c)
        newstring += chr(c)
    return newstring


# Function for checking the parity of received messages.
def check_parity(string):
    newstring = ""
    for char in string:
        c = ord(char)
        lsb = c & 1
        c >>= 1
        if lsb != get_parity(c):
            return False
        newstring += chr(c)
    return newstring


# Function for splitting the message (if longer than 64 bytes) into multiple parts.
def pieces(message):
    partlist = []
    parts, remainder = divmod(len(message), 64)
    for i in range(parts):
        partlist.append(message[i*64:i*64+64])
    if remainder != 0:
        partlist.append(message[parts*64:parts*64+remainder])
    return partlist


# Main function
def main():
    usage = 'usage: %s <server address> <server port> <message>' % sys.argv[0]
 
    try:
        # Get the server address, port and message from command line arguments.
        server_address = str(sys.argv[1])
        server_tcpport = int(sys.argv[2])
        message = str(sys.argv[3])
    except IndexError:
        print("Index Error")
    except ValueError:
        print("Value Error")
    # Print usage instructions and exit if we didn't get proper arguments.
        sys.exit(usage)
 
    send_and_receive_tcp(server_address, server_tcpport, message)
 
 
if __name__ == '__main__':
    # Call the main function when this script is executed.
    main()
